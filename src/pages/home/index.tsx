import React, { useEffect } from "react";
import { connect } from "react-redux";
import * as types from "../../redux/types";
import Header from "../../components/header";
import HeaderCard from "../../components/header-card";
import Card from "../../components/card";
import NewCard from "../../components/new-card";

function Home(props: any) {
  useEffect(() => {
    props.getShoppingList();

    // eslint-disable-next-line
  }, []);
  return (
    <div>
      <Header />
      <div className="main-part">
        <HeaderCard />
      </div>
      <div className="card_view_layout">
        {props.payload &&
          props.payload.map((value: any, index: number) => (
            <div>
              <Card index={index} />
            </div>
          ))}
      </div>
    </div>
  );
}

const mapStateToProps = (state: any) => ({
  ...state.shoppingReducer,
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    getShoppingList: () =>
      dispatch({
        type: types.GET_ALL_SHOPPERS_LIST,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
