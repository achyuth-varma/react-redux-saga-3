import { all } from "redux-saga/effects";
import { watchingShopper } from "./shopper-saga";

export default function* rootSaga() {
  yield all([watchingShopper()]);
}
