import * as types from "../types";
import { call, put, takeLatest } from "redux-saga/effects";
import { ServerResponse } from "http";

const apiUrl = "https://fakestoreapi.com/products";

function getApi() {
  return fetch(apiUrl, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .catch((error) => {
      throw error;
    });
}

function* callBackFunction() {
  try {
    // Calling
    const shoppingList: ServerResponse = yield call(getApi);

    yield put({
      type: types.GET_ALL_SHOPPERS_LIST_ASYNC,
      payload: shoppingList,
    });
  } catch (error) {
    yield put({
      type: types.GET_ALL_SHOPPERS_LIST_ASYNC_FAILED,
      message: error.message,
    });
  }
}

function* watchingShopper() {
  yield takeLatest(types.GET_ALL_SHOPPERS_LIST, callBackFunction);
}

export { watchingShopper };
