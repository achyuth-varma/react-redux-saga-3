import { combineReducers } from "redux";
import shoppingReducer from "./shopper-reducer";

const rootReducer = combineReducers({ shoppingReducer });

export default rootReducer;
