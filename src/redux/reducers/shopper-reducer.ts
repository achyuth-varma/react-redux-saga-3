import * as types from "../types";

interface actionType {
  type: string;
  payload?: string;
  message?: string;
}

export default function shoppingReducer(state = {}, action: actionType) {
  switch (action.type) {
    case types.GET_ALL_SHOPPERS_LIST_ASYNC:
      return {
        ...state,
        payload: action.payload,
      };
    case types.GET_ALL_SHOPPERS_LIST_ASYNC_FAILED:
      return {
        ...state,
        message: action.message,
      };

    default:
      return state;
  }
}
