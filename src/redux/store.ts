import { composeWithDevTools } from "redux-devtools-extension";
import { createStore, applyMiddleware } from "redux";
import mainReducer from "./reducers/main-reducer";
import sagaMiddleWare from "redux-saga";
import mainSagaRoot from "./saga/root-saga";

const theSagaMiddleWare = sagaMiddleWare();

const store = createStore(
  mainReducer,
  composeWithDevTools(applyMiddleware(theSagaMiddleWare))
);

theSagaMiddleWare.run(mainSagaRoot);

export default store;
