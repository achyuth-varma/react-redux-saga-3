import React from "react";
import { connect } from "react-redux";

interface propType {
  index: number;
  payload?: any;
}

export const NewCard = (props: propType) => {
  return (
    <div>
      {props.payload && (
        <div>
          <div
            style={{
              width: "20rem",
              display: "flex",
              justifyContent: "space-evenly",
            }}
          >
            <img
              src={props.payload[props.index].image}
              style={{ width: "100%" }}
            />
          </div>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  ...state.shoppingReducer,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(NewCard);
