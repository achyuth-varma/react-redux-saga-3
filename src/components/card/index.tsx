import React from "react";
import { connect } from "react-redux";

interface propInterface {
  index: number;
  payload?: any;
}

export const Card = (props: propInterface) => {
  return (
    <div>
      {props.payload && (
        <div className="card">
          <div className="card__side card__side--front">
            <div>
              <img
                src={props.payload[props.index].image}
                alt="Product_Image"
                style={{ width: "100%" }}
              />
              <hr></hr>
              <div style={{ color: "black" }}>
                {props.payload[props.index].title}
              </div>
            </div>
          </div>
          <div className="card__side card__side--back">
            <div>Title: {props.payload[props.index].title}</div>
            <hr></hr>
            <div>Price: {props.payload[props.index].price}</div>
            <hr></hr>
            {/* <div>Description: {props.payload[props.index].description}</div>
            <hr></hr> */}
            <div>Category: {props.payload[props.index].category}</div>
          </div>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  ...state.shoppingReducer,
});

const mapDispatchToProps = (dispatch: any) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Card);
