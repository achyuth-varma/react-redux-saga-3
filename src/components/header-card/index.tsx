import React from "react";

function HeaderCard() {
  return (
    <div>
      <div className="image_block">
        <div className="header_text-box">
          <h1 className="heading-primary">
            <span className="heading-primary--name">Retail</span>
            <span className="heading-primary--sub">Retail Stores</span>
          </h1>
        </div>
      </div>
    </div>
  );
}

export default HeaderCard;
