import React, { useEffect } from "react";
import { connect } from "react-redux";
import * as types from "../../redux/types";

function Header(props: any) {
  useEffect(() => {
    props.getShoppingList();

    // eslint-disable-next-line
  }, []);

  return (
    <div className="main_header">
      <div className="main_header__name">Achyuth Varma</div>
      <div className="main_header__signout">Sign Out</div>
    </div>
  );
}

const mapStateToProps = (state: any) => ({
  ...state.shoppingReducer,
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    getShoppingList: () =>
      dispatch({
        type: types.GET_ALL_SHOPPERS_LIST,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
